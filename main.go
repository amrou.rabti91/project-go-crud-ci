package main

import (
	"html/template"
	"net/http"
)

// Page represents the data for the template
type Page struct {
	Title string
}

func main() {
	// Handle the form request
	http.HandleFunc("/submit", submitHandler)

	// Handle the main page
	http.HandleFunc("/", indexHandler)

	// Start the server
	http.ListenAndServe(":9090", nil)
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	// Render the main page template
	page := Page{Title: "Simple Go Form"}
	renderTemplate(w, "index", page)
}

func submitHandler(w http.ResponseWriter, r *http.Request) {
	// Process the form submission here (you can add your logic)

	// For simplicity, let's just print the submitted data
	r.ParseForm()
	name := r.Form.Get("name")
	message := r.Form.Get("message")
	println("Name:", name)
	println("Message:", message)

	// Redirect back to the main page
	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func renderTemplate(w http.ResponseWriter, tmpl string, p Page) {
	// Load the HTML template
	tmplPath := "templates/" + tmpl + ".html"
	t, err := template.ParseFiles(tmplPath)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Execute the template with the data
	err = t.Execute(w, p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
